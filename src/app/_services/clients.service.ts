import { Injectable } from '@angular/core';
import { ClientsModel } from '../_models/clients.model';
import { HttpClient } from '@angular/common/http';
import {ProductModel} from "../_models/product.model";

@Injectable({
  providedIn: 'root'
})
export class ClientsService {
  public clients: ClientsModel[] = [];

  constructor(private http: HttpClient) { }

  public getClients() {
    return this.http.get('http://localhost:3001/api/v1/clients');
  }

  public addClient(client: ClientsModel) {
    return this.http.post('http://localhost:3001/api/v1/clients', client);
  }

  public removeClient(id: string) {
      return this.http.delete(`http://localhost:3001/api/v1/clients/${id}`);
  }

  public changeClient(id: string, client: ClientsModel) {
      return this.http.put(`http://localhost:3001/api/v1/clients/${id}`, client);
  }
}
