import { Injectable } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import { ProductModel } from "../_models/product.model";

@Injectable({
  providedIn: 'root'
})
export class ProductionsService {
  public products: ProductModel[] = [];

  constructor(private http: HttpClient) {}

  public getProducts() {
    return this.http.get('http://localhost:3001/api/v1/products');
  }

  public addProduct(product: ProductModel) {
    return this.http.post('http://localhost:3001/api/v1/products', product);
  }

  public removeProduct(id: string) {
    return this.http.delete(`http://localhost:3001/api/v1/products/${id}`);
  }

  public changeProduct(id: string, product: ProductModel) {
      return this.http.put(`http://localhost:3001/api/v1/products/${id}`, product);
  }
}
