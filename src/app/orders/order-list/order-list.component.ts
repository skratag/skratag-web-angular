import { Component, OnInit } from '@angular/core';
import { OrderService } from "../../_services/orders.service";
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import {AddOrderComponent} from "./add-order/add-order.component";

@Component({
  selector: 'app-order-list',
  templateUrl: './order-list.component.html',
  styleUrls: ['./order-list.component.scss']
})
export class OrderListComponent implements OnInit {
  modalAdd: BsModalRef;

  constructor(
      public orderService: OrderService,
      private modalService: BsModalService
  ) { }

  ngOnInit() {
    this.orderService.getOrders().subscribe(orders => {
      this.orderService.orders = orders['orders'];
    })
  }

  openAddModal() {
    this.modalAdd = this.modalService.show(AddOrderComponent);
  }
}
