import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { HttpClientModule } from '@angular/common/http';
import { ReactiveFormsModule } from '@angular/forms';

import { ModalModule } from 'ngx-bootstrap';

import { RouterModule } from '@angular/router';
import { ROUTES } from './app.routes';

import { LayoutComponent } from './layout/layout.component';
import { ClientsListComponent } from './clients-list/clients-list.component';
import { ProductionListComponent } from './production-list/production-list.component';
import { AddProductComponent } from './production-list/add-product/add-product.component';
import { EditProductComponent } from './production-list/edit-product/edit-product.component';
import { AddClientComponent } from './clients-list/add-client/add-client.component';
import { EditClientComponent } from './clients-list/edit-client/edit-client.component';

@NgModule({
  declarations: [
    AppComponent,
    ClientsListComponent,
    LayoutComponent,
    ProductionListComponent,
    AddProductComponent,
    EditProductComponent,
    AddClientComponent,
    EditClientComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    ReactiveFormsModule,

    RouterModule.forRoot(ROUTES),
    ModalModule.forRoot()
  ],
  providers: [ ],
  bootstrap: [ AppComponent ],
  entryComponents: [
      AddProductComponent,
      EditProductComponent,
      AddClientComponent,
      EditClientComponent
  ]
})
export class AppModule { }
