import { Routes } from '@angular/router';
import { OrderListComponent } from "./order-list/order-list.component";

export const ROUTES: Routes = [
    {
        path: '',
        component: OrderListComponent
    }
];