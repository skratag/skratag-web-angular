import { Component, OnInit } from '@angular/core';
import { ProductionsService } from "../_services/productions.service";
import { AddProductComponent } from "./add-product/add-product.component";
import { EditProductComponent } from "./edit-product/edit-product.component";
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import _ from 'lodash';

@Component({
    selector: 'app-production-list',
    templateUrl: './production-list.component.html',
    styleUrls: ['./production-list.component.scss'],
    providers: [ BsModalService ]
})
export class ProductionListComponent implements OnInit {
    public addModalRef: BsModalRef;

    constructor(
        public productionService: ProductionsService,
        private modalService: BsModalService
    ) { }

    ngOnInit() {
        this.productionService.getProducts().subscribe(products => {
            this.productionService.products = products['products'];
        });
    }

    copyText(id: string) {
        let product = _.find(this.productionService.products, { '_id': id });
        let val = `Название: ${product.name}\nЦена: ${product.price }р.\nСостав: ${product.composition}`;
        let selBox = document.createElement('textarea');
        selBox.style.position = 'fixed';
        selBox.style.left = '0';
        selBox.style.top = '0';
        selBox.style.opacity = '0';
        selBox.value = val;
        document.body.appendChild(selBox);
        selBox.focus();
        selBox.select();
        document.execCommand('copy');
        document.body.removeChild(selBox);
    }

    openAddModal() {
        this.addModalRef = this.modalService.show(AddProductComponent);
    }

    removeProduct(id) {
        this.productionService.removeProduct(id).subscribe(response => {
            if(response) {
                this.productionService.products = this.productionService.products.filter(item => {
                    return item._id !== id;
                })
            }
        });
    }

    openChangeModal(id) {
        this.addModalRef = this.modalService.show(EditProductComponent, { initialState: _.find(this.productionService.products, { _id: id })});
    }
}
