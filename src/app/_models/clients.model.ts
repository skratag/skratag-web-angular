import { BaseModel } from "./base.model";

export class ClientsModel extends BaseModel {
    name: string;
    email: string;
    phone: string;
    comment: string;
}