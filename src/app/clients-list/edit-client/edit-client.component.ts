import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { ClientsService } from "../../_services/clients.service";
import { ClientsModel } from "../../_models/clients.model";
import { BsModalRef } from 'ngx-bootstrap/modal';

@Component({
  selector: 'app-edit-client',
  templateUrl: './edit-client.component.html',
  styleUrls: ['./edit-client.component.scss']
})
export class EditClientComponent implements OnInit {
  editClientForm: FormGroup;
  client: ClientsModel = new ClientsModel();
  _id: string;
  name: string;
  phone: string;
  email: number;
  comment: string;

  constructor(
      private formBuilder: FormBuilder,
      private clientsService: ClientsService,
      public editModalRef: BsModalRef
  ) { }

  ngOnInit() {
      this.initForm();
  }

  initForm() {
      this.editClientForm = this.formBuilder.group({
          name: [this.name],
          phone: [this.phone],
          email: [this.email],
          comment: [this.comment]
      })
  }

  editClient() {
      this.editModalRef.hide();
      const controls = this.editClientForm.controls;
      Object.keys(controls).forEach(controlName => {
          this.client[controlName] = controls[controlName].value;
      });

      this.clientsService.changeClient(this._id, this.client).subscribe(clients => {
          this.clientsService.clients = clients['clients'];
      });
  }

}
