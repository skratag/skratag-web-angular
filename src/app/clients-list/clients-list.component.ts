import { Component, OnInit } from '@angular/core';
import { ClientsService } from '../_services/clients.service';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { AddClientComponent } from "./add-client/add-client.component";
import { EditClientComponent } from "./edit-client/edit-client.component";
import _ from 'lodash';

@Component({
  selector: 'app-clients-list',
  templateUrl: './clients-list.component.html',
  styleUrls: ['./clients-list.component.scss']
})
export class ClientsListComponent implements OnInit {
  public addModalRef: BsModalRef;

  constructor(
    public clientsService: ClientsService,
    private modalService: BsModalService
  ) { }

  ngOnInit() {
    this.clientsService.getClients().subscribe(clients => {
      this.clientsService.clients = clients['clients'];
    })
  }

  removeClient(id) {
      this.clientsService.removeClient(id).subscribe(response => {
          if(response) {
              this.clientsService.clients = this.clientsService.clients.filter(item => {
                  return item._id !== id;
              })
          }
      });
  }

  openAddModal() {
      this.addModalRef = this.modalService.show(AddClientComponent);
  }

    openChangeModal(id) {
        this.addModalRef = this.modalService.show(
            EditClientComponent,
            {
                initialState: _.find(this.clientsService.clients, { _id: id })
            }
        );
    }
}
