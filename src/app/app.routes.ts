import { Routes } from '@angular/router';
import { ClientsListComponent } from './clients-list/clients-list.component';
import { LayoutComponent } from "./layout/layout.component";
import { ProductionListComponent } from "./production-list/production-list.component";

export const ROUTES: Routes = [
    {
        path: '',
        component: LayoutComponent,
        children: [
            { path: '', loadChildren: './orders#OrdersModule' },
            { path: 'orders', loadChildren: './orders#OrdersModule' },
            { path: 'clients', component: ClientsListComponent },
            { path: 'production', component: ProductionListComponent }
        ]
    }
];