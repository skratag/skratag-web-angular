import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { ProductionsService } from "../../_services/productions.service";
import { ProductModel } from "../../_models/product.model";
import { BsModalRef } from 'ngx-bootstrap/modal';

@Component({
    selector: 'app-edit-product',
    templateUrl: './edit-product.component.html',
    styleUrls: ['./edit-product.component.scss']
})

export class EditProductComponent implements OnInit {
    editProductForm: FormGroup;
    product: ProductModel = new ProductModel();
    _id: string;
    name: string;
    description: string;
    price: number;
    composition: string;

    constructor(
        private formBuilder: FormBuilder,
        private productionService: ProductionsService,
        public editModalRef: BsModalRef
    ) { }

    ngOnInit() {
        this.initForm();
    }

    initForm() {
        this.editProductForm = this.formBuilder.group({
            name: [this.name],
            description: [this.description],
            price: [this.price],
            composition: [this.composition]
        })
    }

    editProduct() {
        this.editModalRef.hide();
        const controls = this.editProductForm.controls;
        Object.keys(controls).forEach(controlName => {
            this.product[controlName] = controls[controlName].value;
        });

        this.productionService.changeProduct(this._id, this.product).subscribe(products => {
            this.productionService.products = products['products'];
        });
    }

}
