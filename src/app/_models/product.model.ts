import { BaseModel } from "./base.model";

export class ProductModel extends BaseModel {
    name: string;
    description: string;
    price: number;
    composition: string;
}