import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { ProductionsService } from "../../_services/productions.service";
import { ProductModel } from "../../_models/product.model";
import { BsModalRef } from 'ngx-bootstrap/modal';

@Component({
  selector: 'app-add-product',
  templateUrl: './add-product.component.html',
  styleUrls: ['./add-product.component.scss']
})
export class AddProductComponent implements OnInit {
  addProductForm: FormGroup;
  product: ProductModel = new ProductModel();

  constructor(
      private formBuilder: FormBuilder,
      private productionService: ProductionsService,
      public addModalRef: BsModalRef
  ) { }

  ngOnInit() {
    this.initForm();
  }

  initForm() {
    this.addProductForm = this.formBuilder.group({
        name: [''],
        description: [''],
        price: [''],
        composition: ['']
    })
  }

  addProduct() {
    this.addModalRef.hide();
    const controls = this.addProductForm.controls;
    Object.keys(controls).forEach(controlName => {
      this.product[controlName] = controls[controlName].value;
    });

    this.productionService.addProduct(this.product).subscribe(products => {
        this.productionService.products = products['products'];
    });
  }
}
