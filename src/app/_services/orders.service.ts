import { Injectable } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import { OrderModel } from "../_models/order.model";

@Injectable({
    providedIn: 'root'
})
export class OrderService {
    public orders: OrderModel[] = [];

    constructor(private http: HttpClient) {}

    getOrders() {
        return this.http.get('http://localhost:3001/api/v1/orders');
    }
}
