import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { BsModalRef } from "ngx-bootstrap/modal";
import { ClientsModel } from "../../_models/clients.model";
import { ClientsService } from "../../_services/clients.service";

@Component({
  selector: 'app-add-client',
  templateUrl: './add-client.component.html',
  styleUrls: ['./add-client.component.scss']
})
export class AddClientComponent implements OnInit {
  addClientForm: FormGroup;
  client: ClientsModel = new ClientsModel();

  constructor(
      private formBuilder: FormBuilder,
      private clientsService: ClientsService,
      public addModalRef: BsModalRef
  ) { }

  ngOnInit() {
    this.initForm();
  }

  initForm() {
      this.addClientForm = this.formBuilder.group({
          name: [''],
          phone: [''],
          email: [''],
          comment: ['']
      })
  }

    addClient() {
        this.addModalRef.hide();
        const controls = this.addClientForm.controls;
        Object.keys(controls).forEach(controlName => {
            this.client[controlName] = controls[controlName].value;
        });

        this.clientsService.addClient(this.client).subscribe(clients => {
            this.clientsService.clients = clients['clients'];
        });
    }
}
