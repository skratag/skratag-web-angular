import { BaseModel } from "./base.model";
import { ClientsModel } from "./clients.model";
import { ProductModel } from "./product.model";

export class OrderModel extends BaseModel {
    number: string;
    status: string;
    payment: boolean;
    client: ClientsModel;
    products: ProductModel[]
}