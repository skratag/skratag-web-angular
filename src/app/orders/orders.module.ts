import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';

import { ROUTES } from './orders.routes';

import { OrderListComponent } from './order-list/order-list.component';
import { AddOrderComponent } from './order-list/add-order/add-order.component';

@NgModule({
    imports: [
        CommonModule,
        RouterModule.forChild(ROUTES)
    ],
    declarations: [
        OrderListComponent,
        AddOrderComponent
    ],
    entryComponents: [
        AddOrderComponent
    ]
})
export class OrdersModule { }
